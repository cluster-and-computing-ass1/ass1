#!/usr/bin/env python
# coding: utf-8

# <h1> The dummy program to pre-process the tiny .json file </h1>

# Data Pre-processing 

# In[1]:


import json
import pandas as pd
#Loading .json file
data_load=json.load(open("smallTwitter.json", encoding='utf-8'))
"""
The json.load file will first load the json data as dict. Here we use panda.DataFrame transformming all dict
keys to columns of the dataframes.
"""
#loading json file
data_load=data_load['rows']

#Defining useful functions
def create_dataframe(data):
    """transforming loaded json dict to dataframe
    Args:
        data:source json dict
    Returns:
        The created dataframe
    """
    out= pd.DataFrame(columns=data[0].keys())
    for i in range(len(data)):
        out=out.append(data[i],ignore_index=True)
    return out

def json_to_columns(df,col_name):
    """Unpacking json, one layer each time
    Args:
        df:target dataframe
        col_name: col_name of dataframe
    Returns:
        The transformed dataframe
    """
    for i in df[col_name][0].keys(): # unpacking dict
        temp_list=[j[i] for j in df[col_name]] #loading all value into temp list
        df[i]=temp_list # adding new column      
    df.drop(col_name,axis=1,inplace=True) #deleting original column
    return df

def json_parse(df):
    """Searching through whole dataframe, deal with all type(dict) column
    Args:
        df:target dataframe
    Returns:
        The transformed dataframe
    """
    for i in df.keys():
        if type(df[i][0])==dict and df[i][0]!={}:
            df = json_to_columns(df,i)
    return df    
        


# In[2]:


#creating data frame from the loaded data source, and extract useful features
data_raw = create_dataframe(data_load)
data_raw=json_parse(data_raw)

#deleting the rows that does not include geo or language info
data_raw.drop(data_raw[pd.isnull(data_raw['coordinates'])].index,inplace=True)
data_raw.drop(data_raw[pd.isnull(data_raw['lang'])].index,inplace=True)

#slicing useful features
data_filtered = data_raw.loc[:,['id','coordinates','lang','location']]
data_filtered 


# By observing the pins on the google map, create the dict of feature id and the corresponding grid id

# In[3]:


grid={'23':'C4','22':'B4','21':'A4','20':'D3','19':'C3','18':'B3','17':'A3','16':'D2','15':'C2','14':'B2','13':'A2','12':'D1',      '11':'C1','10':'B1','9':'A1','24':'D4'}
#loading grid file
grid_load=json.load(open("sydGrid-2.json", encoding='utf-8'))
grid_data=create_dataframe(grid_load['features'])
grid_data=json_parse(grid_data)
grid_data


# """
# import sys
# !{sys.executable} -m pip install shapely
# """

# In[4]:


from shapely import geometry
def assign_grid(lo,la):
    """Assigning tweets to specific grid block
    Args:
        lo:longtitude
        la:latitude
    Returns:
        grid block id (For instance, A1)
    """
    def if_inPoly(polygon,Point):
        """Whether the given point is in the specified polygon
        Args:
            polygon:polygon defined by four sets of coordinates
            la:latitude
        Returns:
            True or Fale
        """ 
        line=geometry.LineString(polygon)
        point=geometry.Point(Point)
        polygon=geometry.Polygon(line)
        return polygon.contains(point)
    
    def fix_outlier(lo,la):
        """Whether the given point is out of all grid polygon
        Args:
           lo:longitude
           la:latitude 
        Returns:
            Adjusted longtitude and latitude
        """ 
        #leftover
        if lo<=150.7655:
            lo=150.79
        #above
        if la>-33.55412:
            la=-33.6
        #rightover
        if lo>151.3655:
            lo=151.35
        #bottom
        if la<-34.00412:
            la=-33.95
        return lo,la
    
    lo,la=fix_outlier(lo,la)
    p=(-la,lo)
    for i in range(len(grid_data['coordinates'])):
        square=[]
        for j in grid_data['coordinates'][i][0]:
            square.append((-j[1],j[0])) 
            #If on longtitude edge,assign it to the grid on the left
            if lo==j[0] and la!=j[1]:
                del p #delete original tuple
                p=(-la,lo-0.05)#send it to the grid on the left
            #If on latitude edge,assign it to the grid below
            if la==j[1] and lo!=j[0]:
                del p #delete original tuple
                p=(-la-0.05,lo)#send it to the grid below
            #If on the exact point,send it to the left_down grid
            if lo==j[0] and la==j[1]:
                del p #delete original tuple
                p=(-la-0.05,lo-0.05)#send it to the grid on the left_down
                
        #find the correct polygon,return grid id
        if if_inPoly(square,p):
            key=str(grid_data['id'][i])  
            return(grid[key])


#  Grid catogorization 

# In[5]:


temp=[] #store grid values
for p in data_filtered['coordinates']:
    temp.append(assign_grid(p['coordinates'][0],p['coordinates'][1]))
data_filtered['Grid']=temp #adding new column
data_filtered


# In[6]:


from collections import defaultdict
from collections import Counter
import operator

l1=list(data_filtered['Grid'])
l2=list(data_filtered['lang'])

#Get the results set
lan_count=defaultdict(Counter) #counting numbers of tweets of different languages
grid_count=defaultdict(int)
for i in range(len(l1)):
    lan_count[l1[i]][l2[i]]+=1
    grid_count[l1[i]]+=1


# Using the results set to generate the final output

# In[7]:


output= pd.DataFrame(columns=['Cell','#Total_Tweets','#Number_of_languages','#Top10_languages_of_tweets'])
output['Cell']=lan_count.keys()
output['#Total_Tweets']=grid_count.values() 
output['#Number_of_languages']=[len(lan_count[key]) for key in lan_count.keys()]
temp_list=[]
for value in lan_count.values():
    for x,y in value.items():
        temp_list.append(x+"-"+str(y))
output['#Top10_languages_of_tweets']=temp_list


# In[8]:


output


# In[ ]:




