from mpi4py import MPI
from shapely import geometry
import numpy as np
import re
import ijson
import jsonpath
import orjson
import pandas as pd
from collections import defaultdict
from collections import Counter
from tkinter import _flatten
pd.set_option('display.max_columns',1000)
pd.set_option('display.width',1000)
pd.set_option('display.max_colwidth',1000)
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
tra_lang={'en':'English','ar':'Arabic','bn':'Czech','da':'Danish','de':'German','el':'Greek',\
          'es':'Spanish','fa':'Persian','fi':'Finish','fil':'Filipino','fr':'French','he':'Hebrew',\
          'hi':'Hindi','hu':'Hungarian','id':'Indonesian','it':'Italian','ja':'Japanese','ko':'Korean',\
          'msa':'Malay','nl':'Dutch','no':'Norwegian','pl':'Polish','pt':'Portuguese','ro':'Romanian','ru':'Russian',\
          'sv':'Swedish','th':'Thai','tr':'Turkish','uk':'Ukrainian','ur':'Urdu','vi':'Vietnamese','zh':'Chinese'}
"""
Package needed: These packages need to be pre-installed and loaded from slrum.
json,panda,jsonpath,panda,collections
orjson[4 times faster than json.loads],
ijson[parsing json file with streams]
"""

def create_dataframe(data):
    """transforming loaded json dict to dataframe
    Args:
        data:source json dict
    Returns:
        The created dataframe
    """
    out= pd.DataFrame(columns=data[0].keys())
    for i in range(len(data)):
        out=out.append(data[i],ignore_index=True)
    return out

def json_to_columns(df,col_name):
    """Unpacking json, one layer each time
    Args:
        df:target dataframe
        col_name: col_name of dataframe
    Returns:
        The transformed dataframe
    """
    for i in df[col_name][0].keys(): # unpacking dict
        temp_list=[j[i] for j in df[col_name]] #loading all value into temp list
        df[i]=temp_list # adding new column      
    df.drop(col_name,axis=1,inplace=True) #deleting original column
    return df

def json_parse(df):
    """Searching through whole dataframe, deal with all type(dict) column
    Args:
        df:target dataframe
    Returns:
        The transformed dataframe
    """
    for i in df.keys():
        if type(df[i][0])==dict and df[i][0]!={}:
            df = json_to_columns(df,i)
    return df    

def assign_grid(lo,la):
    """Assigning tweets to specific grid block
    Args:
        lo:longtitude
        la:latitude
    Returns:
        grid block id (For instance, A1)
    """
    def fix_outlier(lo,la):
        """Whether the given point is out of all grid polygon
        Args:
           lo:longitude
           la:latitude 
        Returns:
            Adjusted longtitude and latitude
        """ 
        #leftover
        if lo<=150.7655:
            lo=150.79
        #above
        if la>-33.55412:
            la=-33.6
        #rightover
        if lo>151.3655:
            lo=151.35
        #below
        if la<-34.15412:
            la=-34.12
        return lo,la 
    #Assigning grid
    lo,la=fix_outlier(lo,la)
    if longtitudes[3]<lo<=longtitudes[4] and latitudes[0]<=la<=latitudes[1]:
        return 'D4'
    elif longtitudes[3]<lo<=longtitudes[4] and latitudes[1]<la<=latitudes[2]:
        return 'C4'
    elif longtitudes[3]<lo<=longtitudes[4] and latitudes[2]<la<=latitudes[3]:
        return 'B4'
    elif longtitudes[3]<lo<=longtitudes[4] and latitudes[3]<la<=latitudes[4]:
        return 'A4'
    elif longtitudes[2]<lo<=longtitudes[3] and latitudes[0]<=la<=latitudes[1]:
        return 'D3'    
    elif longtitudes[2]<lo<=longtitudes[3] and latitudes[1]<la<=latitudes[2]:
        return 'C3'
    elif longtitudes[2]<lo<=longtitudes[3] and latitudes[2]<la<=latitudes[3]:
        return 'B3'
    elif longtitudes[2]<lo<=longtitudes[3] and latitudes[3]<la<=latitudes[4]:
        return 'A3'
    elif longtitudes[1]<lo<=longtitudes[2] and latitudes[0]<=la<=latitudes[1]:
        return 'D2'
    elif longtitudes[1]<lo<=longtitudes[2] and latitudes[1]<la<=latitudes[2]:
        return 'C2'
    elif longtitudes[1]<lo<=longtitudes[2] and latitudes[2]<la<=latitudes[3]:
        return 'B2'
    elif longtitudes[1]<lo<=longtitudes[2] and latitudes[3]<la<=latitudes[4]:
        return 'A2'
    elif longtitudes[0]<=lo<=longtitudes[1] and latitudes[0]<=la<=latitudes[1]:
        return 'D1'
    elif longtitudes[0]<=lo<=longtitudes[1] and latitudes[1]<la<=latitudes[2]:
        return 'C1'
    elif longtitudes[0]<=lo<=longtitudes[1] and latitudes[2]<la<=latitudes[3]:
        return 'B1' 
    elif longtitudes[0]<=lo<=longtitudes[1] and latitudes[3]<la<=latitudes[4]:
        return 'A1'  

def update_dic(l_grid,l_lang,lan_count,grid_count):
    """update the result dictionaries
    Args: l_gird:result_grid, l_lang:result_lang 
    Returns:updated dic
    """  
    #counting values           
    for i in range(len(l_grid)):
        lan_count[l_grid[i]][l_lang[i]]+=1
        grid_count[l_grid[i]]+=1
    return lan_count,grid_count
    
def update_list(result_grid,result_lang,lo,la):
    grid=assign_grid(lo,la)
    print(grid)
    result_grid.append(grid)
    result_lang.append(lang)
      
#loading grid file
with open("sydGrid.json", encoding='utf-8') as f1:
    grid_load=orjson.loads(f1.read())
    #Getting all longtitudes and latitudes
    latitudes=sorted(set([grid_load['features'][i]['geometry']['coordinates'][0][1][1] \
                for i in range(len(grid_load['features']))]+\
                    [grid_load['features'][i]['geometry']['coordinates'][0][3][1] \
                for i in range(len(grid_load['features']))]))
    longtitudes=sorted(set([grid_load['features'][i]['geometry']['coordinates'][0][1][0] \
                for i in range(len(grid_load['features']))]+
                    [grid_load['features'][i]['geometry']['coordinates'][0][2][0] \
                for i in range(len(grid_load['features']))]))
    
#loading data
i=0 #counter
lan_count=defaultdict(Counter) #counting numbers of tweets of different languages
grid_count=defaultdict(int)
send_obj=[]
result_grid=[] #default result:None
result_lang=[]#default result:None
#reading data
f2=open('bigTwitter.json','r',encoding='utf-8')
raw = ijson.kvitems(f2,'rows.item')
data_useful=(v for k,v in raw if k=='doc')
for data in data_useful:
    if rank==0:
        send_obj.append(data)
        i+=1
    else:
        send_obj=None
        i+=1
    if i%size==0:
        recv_obj=comm.scatter(send_obj,root=0)
        send_obj=[]
        lang=jsonpath.jsonpath(recv_obj,'$.lang')[0]
        #Example of dealing with exemptions like this
        if lang == 'zh-cn' or lang == 'zh-tw':
            lang = 'zh'
        coor=jsonpath.jsonpath(recv_obj,'$.coordinates.coordinates')           
        if lang and coor:
            lo=float(coor[0][0])
            la=float(coor[0][1])
            grid=assign_grid(lo,la)
            result_grid.append(grid)
            result_lang.append(lang)
f2.close()

##Deal with leftovers,only rank 0's will possibly be unempty
if rank==0:
    if send_obj:
        for obj in send_obj:
            lang=jsonpath.jsonpath(obj,'$.lang')[0]
            coor=jsonpath.jsonpath(obj,'$.coordinates.coordinates')
            if lang and coor:
                for i in range(len(lang)):
                    grid=assign_grid(lo,la)
                    result_grid.append(grid)
                    result_lang.append(lang)
            
#counting values           
for i in range(len(result_grid)):
    lan_count[result_grid[i]][result_lang[i]]+=1
    grid_count[result_grid[i]]+=1
    
#Collecting results    
recv_grid=comm.gather(grid_count,root=0)
recv_lang=comm.gather(lan_count,root=0)
 
# updating headnode dict
if rank==0:
    recv_grid.pop(0) #The first one is from itself, pop out
    recv_lang.pop(0)
    #updating tweets counting dict
    for recv in recv_grid:
        for key,value in recv.items():
            grid_count[key]+=value
    #updating language counting dict
    for recv in recv_lang:
        for grid,dic in recv.items():
            for key,value in dic.items():
                lan_count[grid][key]+=value
    #output
    output= pd.DataFrame(columns=['Cell','#Total_Tweets','#Number_of_languages','#Top10_languages_of_tweets'])
    output['Cell']=lan_count.keys()
    output['#Total_Tweets']=grid_count.values() 
    output['#Number_of_languages']=[len(lan_count[key]) for key in lan_count.keys()]
    temp_list=[[]for i in range(len(lan_count))]
    print(len(temp_list))
    i=0 #counter
    for value in lan_count.values():
        sorted_value=sorted(value.items(), key = lambda kv:(kv[1], kv[0]),reverse=True) #sorted dict based on value
        sorted_value=sorted_value[:10] # 10 most languages
        for lan,count in sorted_value:
            temp_list[i].append("%s-%d"%(tra_lang[lan],count))
        i+=1
    output['#Top10_languages_of_tweets']=temp_list
    output.sort_values("Cell",inplace=True)
    print(output)



