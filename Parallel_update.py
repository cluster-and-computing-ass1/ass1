from mpi4py import MPI
from shapely import geometry
import numpy as np
import re
import ijson
import jsonpath
import orjson
import pandas as pd
from collections import defaultdict
from collections import Counter
from tkinter import _flatten

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
"""
Package needed: These packages need to be pre-installed and loaded from slrum.
json,panda,jsonpath,panda,collections
orjson[4 times faster than json.loads],
ijson[parsing json file with streams]
"""

def create_dataframe(data):
    """transforming loaded json dict to dataframe
    Args:
        data:source json dict
    Returns:
        The created dataframe
    """
    out= pd.DataFrame(columns=data[0].keys())
    for i in range(len(data)):
        out=out.append(data[i],ignore_index=True)
    return out

def json_to_columns(df,col_name):
    """Unpacking json, one layer each time
    Args:
        df:target dataframe
        col_name: col_name of dataframe
    Returns:
        The transformed dataframe
    """
    for i in df[col_name][0].keys(): # unpacking dict
        temp_list=[j[i] for j in df[col_name]] #loading all value into temp list
        df[i]=temp_list # adding new column      
    df.drop(col_name,axis=1,inplace=True) #deleting original column
    return df

def json_parse(df):
    """Searching through whole dataframe, deal with all type(dict) column
    Args:
        df:target dataframe
    Returns:
        The transformed dataframe
    """
    for i in df.keys():
        if type(df[i][0])==dict and df[i][0]!={}:
            df = json_to_columns(df,i)
    return df    


def assign_grid(lo,la):
    """Assigning tweets to specific grid block
    Args:
        lo:longtitude
        la:latitude
    Returns:
        grid block id (For instance, A1)
    """
    def if_inPoly(polygon,Point):
        """Whether the given point is in the specified polygon
        Args:
            polygon:polygon defined by four sets of coordinates
            la:latitude
        Returns:
            True or Fale
        """ 
        line=geometry.LineString(polygon)
        point=geometry.Point(Point)
        polygon=geometry.Polygon(line)
        return polygon.contains(point)
    
    def fix_outlier(lo,la):
        """Whether the given point is out of all grid polygon
        Args:
           lo:longitude
           la:latitude 
        Returns:
            Adjusted longtitude and latitude
        """ 
        #leftover
        if lo<=150.7655:
            lo=150.79
        #above
        if la>-33.55412:
            la=-33.6
        #rightover
        if lo>151.3655:
            lo=151.35
        #bottom
        if la<-34.00412:
            la=-33.95
        return lo,la
    
    lo,la=fix_outlier(lo,la)
    p=(-la,lo)
    for i in range(len(grid_data['coordinates'])):
        square=[]
        for j in grid_data['coordinates'][i][0]:
            square.append((-j[1],j[0])) 
            #If on longtitude edge,assign it to the grid on the left
            if lo==j[0] and la!=j[1]:
                del p #delete original tuple
                p=(-la,lo-0.05)#send it to the grid on the left
            #If on latitude edge,assign it to the grid below
            if la==j[1] and lo!=j[0]:
                del p #delete original tuple
                p=(-la-0.05,lo)#send it to the grid below
            #If on the exact point,send it to the left_down grid
            if lo==j[0] and la==j[1]:
                del p #delete original tuple
                p=(-la-0.05,lo-0.05)#send it to the grid on the left_down
                
        #find the correct polygon,return grid id
        if if_inPoly(square,p):
            key=str(grid_data['id'][i])  
            return(grids[key])

#loading grid data
grids={'23':'C4','22':'B4','21':'A4','20':'D3','19':'C3','18':'B3','17':'A3','16':'D2','15':'C2','14':'B2','13':'A2','12':'D1',\
      '11':'C1','10':'B1','9':'A1','24':'D4'}
#loading grid file
with open("sydGrid-2.json", encoding='utf-8') as f1:
    grid_load=orjson.loads(f1.read())
    grid_data=create_dataframe(grid_load['features'])
    grid_data=json_parse(grid_data)
    
comm.barrier() #Synchronize

#loading data
i=0
lan_count=defaultdict(Counter) #counting numbers of tweets of different languages
grid_count=defaultdict(int)
send_obj=[]
result_grid=[] #default result:None
result_lang=[]#default result:None
f2=open('smallTwitter.json','r',encoding='utf-8')
raw = ijson.kvitems(f2,'rows.item')
data_useful=(v for k,v in raw if k=='doc')
for data in data_useful:
    if rank==0:
        send_obj.append(data)
        i+=1
    else:
        send_obj=None
        i+=1
    if i%size==0:
        recv_obj=comm.scatter(send_obj,root=0)
        send_obj=[]
        lang=jsonpath.jsonpath(recv_obj,'$.lang')[0]
        coor=jsonpath.jsonpath(recv_obj,'$.coordinates.coordinates')           
        if lang and coor:
            lo=float(coor[0][0])
            la=float(coor[0][1])
            grid=assign_grid(lo,la)
            result_grid.append(grid)
            result_lang.append(lang)
f2.close()
##Deal with leftovers,only rank 0's will possibly be unempty
if rank==0:
    if send_obj:
        for obj in send_obj:
            lang=jsonpath.jsonpath(obj,'$.lang')[0]
            coor=jsonpath.jsonpath(obj,'$.coordinates.coordinates')
            if lang and coor:
                for i in range(len(lang)):
                    lo=float(coor[i][0][0])
                    la=float(coor[i][0][1])
                    grid=assign_grid(lo,la)
                    result_grid.append(grid)
                    result_lang.append(lang)
            
#counting values           
for i in range(len(result_grid)):
    lan_count[result_grid[i]][result_lang[i]]+=1
    grid_count[result_grid[i]]+=1
    
#Collecting results    
recv_grid=comm.gather(grid_count,root=0)
recv_lang=comm.gather(lan_count,root=0)
 
# updating headnode dict
if rank==0:
    recv_grid.pop(0) #The first one is from itself, pop out
    recv_lang.pop(0)
    #updating tweets counting dict
    for recv in recv_grid:
        for key,value in recv.items():
            grid_count[key]+=value
    #updating language counting dict
    for recv in recv_lang:
        for grid,dic in recv.items():
            for key,value in dic.items():
                lan_count[grid][key]+=value
    #output
    output= pd.DataFrame(columns=['Cell','#Total_Tweets','#Number_of_languages','#Top10_languages_of_tweets'])
    output['Cell']=lan_count.keys()
    output['#Total_Tweets']=grid_count.values() 
    output['#Number_of_languages']=[len(lan_count[key]) for key in lan_count.keys()]
    temp_list=[[]for i in range(len(lan_count))]
    i=0 #counter
    for value in lan_count.values():
        sorted_value=sorted(value.items(), key = lambda kv:(kv[1], kv[0]),reverse=True) #sorted dict based on value
        sorted_value=sorted_value[:10] # 10 most languages
        for lan,count in sorted_value:
            temp_list[i].append("%s-%d"%(lan,count))
            i+=1
    output['#Top10_languages_of_tweets']=temp_list    
    print(output)

"""
#Collectively loading data
filename = 'tinyTwitter.json'

start=time.time()

# open the file
fh = MPI.File.Open(comm, filename, amode= MPI.MODE_RDONLY)
file_size=fh.Get_size()
# set to be atomicity mode to avoid read/write conflict
fh.Set_atomicity(True)
# set file view, use MPI.CHAR as etype and filetype
fh.Set_view(0, MPI.CHAR,MPI.CHAR)
buf = np.chararray(100) #Initializing buffer
fh.Read_all(buf)
fh.Close()
"""

